# AsyncSwift Web Server

This is a simple project to mimic a JSON API Server, providing the set of 100 Top Popular Movies (according to iMDB) with some related metadata and images. 

## Installing & running

1. Download and install [Poetry](https://python-poetry.org);

2. Install the project packages:
    ```
    > poetry shell
    > poetry install
    ```

3. Run the server:
    ```
    > ./run.sh 
    ```
