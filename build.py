#!/usr/bin/env python3

import datetime
import os
import re
from typing import TYPE_CHECKING, Dict, Optional, Any
import diskcache


if TYPE_CHECKING:
    import requests


BASE_THUMBNAIL_WIDTH = 64
BASE_POSTER_WIDTH = 300
IMAGE_SCALES = (1, 2, 3)

POSTER_DATA = [
    ('main', BASE_THUMBNAIL_WIDTH, 'thumbnail', 'thumbnail'),
    ('details', BASE_POSTER_WIDTH, 'poster', 'poster'),
]


cache = diskcache.Cache(directory='./cache')


def unescape_html(value: Any) -> Any:
    import html

    if value is None:
        return None
    if isinstance(value, (int, float, bool)):
        return value
    if isinstance(value, str):
        return html.unescape(value)
    if isinstance(value, list):
        return list(map(unescape_html, value))
    if isinstance(value, dict):
        return {unescape_html(k): unescape_html(v) for k, v in value.items()}

    raise ValueError(value)


def load_cacheable_url(session: 'requests.Session', absolute_url: str) -> str:
    if absolute_url in cache:
        return cache[absolute_url]

    print(f'Downloading page {absolute_url}')
    response = session.get(absolute_url)
    text = response.text
    cache[absolute_url] = text
    return text


def get_movie_id(url: str) -> str:
    return re.search(r'/(tt\d+)/?$', url)[1]


def build_image_url(link_url: str, width: int) -> str:
    link_url = re.sub(r'\._V1_.*\.', f'._V1_UX{width}.', link_url)
    return link_url


def download_movie_assets(session: 'requests.Session', movie_id: str, orig_poster_url: str) \
        -> Dict[str, Dict[str, str]]:
    movie_path = os.path.join('pages', movie_id)
    os.makedirs(movie_path, exist_ok=True)
    file_suffix = os.path.splitext(orig_poster_url)[1]

    result = {'main': {}, 'details': {}}

    for group, base_width, url_basename, key in POSTER_DATA:
        for scale in IMAGE_SCALES:
            scaled_width = int(base_width * scale)
            scale_suffix = f'@{scale}x' if scale != 1 else ''
            output_filename = f'{url_basename}{scale_suffix}{file_suffix}'
            asset_path = os.path.join(movie_path, output_filename)
            asset_url = build_image_url(orig_poster_url, scaled_width)
            result[group].setdefault(key, []).append({
                'scale': scale,
                'url': f'/{movie_id}/{output_filename}',
            })

            if os.path.isfile(asset_path) and os.path.getsize(asset_path) > 0:
                continue

            print(f'Downloading {asset_path}')
            response = session.get(asset_url, allow_redirects=True)
            with open(asset_path, 'wb') as fd:
                fd.write(response.content)

    return result


def parse_date_published(value: str, /) -> 'datetime.date':
    return datetime.datetime.strptime(value, '%Y-%m-%d').date()


def parse_duration_minutes(value: Optional[str], /) -> Optional[int]:
    if value is None:
        return None
    if (m := re.fullmatch(r'PT(\d+)H(\d+)M', value)) is not None:
        return int(m[1]) * 60 + int(m[2])
    raise ValueError(value)


def main():
    import json
    import requests
    import bs4
    from urllib.parse import urljoin

    session = requests.Session()

    base_imdb_url = 'https://www.imdb.com/chart/moviemeter/'
    response = session.get(base_imdb_url)
    soup = bs4.BeautifulSoup(response.text, features='html.parser')
    tbody = soup.find(attrs={'class': 'chart'}).find('tbody')

    movies_data = []

    for tr in tbody.find_all('tr'):
        poster_td = tr.find('td', attrs={'class': 'posterColumn'})
        a_el = poster_td.find('a')
        movie_url = urljoin(base_imdb_url, a_el.attrs['href'])
        orig_poster_url = a_el.find('img').attrs['src']
        movie_id = get_movie_id(movie_url)

        rating_td = tr.find('td', attrs={'class': ['ratingColumn', 'imdbRating']})
        imdb_rating = rating_td.text.strip()
        imdb_rating = float(imdb_rating) if imdb_rating else None

        movie_text = load_cacheable_url(session, movie_url)
        movie_json = re.search(r'<script type="application/ld\+json">(.+?)</script>', movie_text)[1]
        movie_json = unescape_html(json.loads(movie_json))

        main_movie_data = {
            'id': movie_id,
            'origTitle': movie_json['name'],
            'altTitle': movie_json.get('alternateName'),
            'year': parse_date_published(movie_json['datePublished']).year,
            'duration': parse_duration_minutes(movie_json.get('duration')),
            'detailsUrl': f'/{movie_id}/index.json',
        }
        details_movie_data = {
            'id': movie_id,
            'imdbRating': imdb_rating,
            'description': movie_json['description'],
            'genres': movie_json['genre'],
            'directors': [item['name'] for item in movie_json['director']],
            'actors': [item['name'] for item in movie_json['actor']],
        }

        d = download_movie_assets(session, movie_id, orig_poster_url)
        main_movie_data |= d['main']
        details_movie_data |= d['details']

        movies_data.append(main_movie_data)

        index_path = os.path.join('pages', movie_id, 'index.json')
        with open(index_path, 'wt', encoding='utf-8') as fd:
            json.dump(details_movie_data, fd, ensure_ascii=False, indent=2)

    index_path = os.path.join('pages', 'index.json')
    with open(index_path, 'wt', encoding='utf-8') as fd:
        json.dump(movies_data, fd, ensure_ascii=False, indent=2)


if __name__ == '__main__':
    main()
